drop procedure if exists projects_tags_fill;

delimiter #
create procedure projects_tags_fill()
begin

	DECLARE current_repo_id INT(11);
    DECLARE current_repo_name VARCHAR(255);
    DECLARE current_description VARCHAR(255) CHARACTER SET latin1;
    
    DECLARE loopDone TINYINT default 0;
    DECLARE tloopDone TINYINT default 0;
    
    DECLARE current_tag_id INT(10);
    DECLARE current_tag_name VARCHAR(45) CHARACTER SET latin1;
    
    DECLARE transformed_description VARCHAR(255) CHARACTER SET latin1;
    DECLARE transformed_tag_name VARCHAR(45) CHARACTER SET latin1;
    
    DECLARE debug_enabled TINYINT default 0;
    SET @debug_enabled = FALSE;

	SET loopDone = 0;
	project_cursor: BEGIN
		DECLARE project_curs CURSOR FOR SELECT `id`,`name`,CONVERT(`description` using 'latin1') FROM `github`.`projects`;
		DECLARE CONTINUE HANDLER FOR NOT FOUND SET loopDone = 1;
        OPEN project_curs;
		call debug_msg(@debug_enabled, "Cursor opened for projects");
		REPEAT
			FETCH project_curs INTO current_repo_id, current_repo_name, current_description;
				IF NOT loopDone THEN
					-- call debug_msg(@debug_enabled, CONCAT("Just fetched a project: ", current_repo_name));
                    
                    SET tloopDone = 0;
                    tag_cursor: begin
						DECLARE tag_curs CURSOR FOR SELECT `id`,CONVERT(`tagname` using 'latin1') FROM `stack`.`tags`;
						DECLARE CONTINUE HANDLER FOR NOT FOUND SET tloopDone = 1;
						OPEN tag_curs;
                        REPEAT
							FETCH tag_curs INTO current_tag_id, current_tag_name;
                            IF NOT tloopDone THEN
								-- try to match
                                set transformed_description = replace(current_description, '+', 'p');
                                set transformed_tag_name = replace(current_tag_name, '+', 'p');
                                set transformed_description = replace(transformed_description, '#', 'h');
                                set transformed_tag_name = replace(transformed_tag_name, '#', 'h');
                                set transformed_description = replace(transformed_description, '-', 'm');
                                set transformed_tag_name = replace(transformed_tag_name, '-', 'm');
                                set transformed_description = replace(transformed_description, '_', 'd');
                                set transformed_tag_name = replace(transformed_tag_name, '_', 'd');
                                set transformed_description = replace(transformed_description, '.', 't');
                                set transformed_tag_name = replace(transformed_tag_name, '.', 't');
                                
                                IF transformed_description REGEXP CONCAT(CONCAT('[[:<:]]',transformed_tag_name),'[[:>:]]') THEN
									
                                    set @export_stmt := 'INSERT INTO `github`.`projects_tags` VALUES (__REPO_ID, __TAG_ID, \'__TAG_NAME\', 1)';
									set @export_stmt := REPLACE(@export_stmt, '__REPO_ID', current_repo_id);
                                    set @export_stmt := REPLACE(@export_stmt, '__TAG_ID', current_tag_id);
                                    set @export_stmt := REPLACE(@export_stmt, '__TAG_NAME', current_tag_name);
									Prepare stmt FROM @export_stmt;
									Execute stmt;
									DEALLOCATE PREPARE stmt;
                                    
                                END IF;
                            END IF;
						UNTIL tloopDone END REPEAT;
                    end tag_cursor;
                    
				ELSE
					call debug_msg(@debug_enabled, "Loop done");
				END IF;
		UNTIL loopDone END REPEAT;
		CLOSE project_curs;
	END project_cursor;

end #
delimiter ;

call projects_tags_fill();