select u.login, ct.tag_name, count(ct.tag_name) as tag_count 
	from commit_comments_tags ct
	inner join commit_comments cc
	on ct.id = cc.id
    join users u
    on cc.user_id = u.id
    group by u.login, ct.tag_name
;