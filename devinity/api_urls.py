from rest_framework import routers
from devinityapp.viewsets import *

api_router = routers.SimpleRouter()
api_router.register(r'api/users', UserViewSet)
api_router.register(r'api/skills', SkillViewSet)
api_router.register(r'api/flag', FlagViewSet)
api_router.register(r'api/flags', FlagSetViewSet)
api_router.register(r'api/organizations', OrganizationViewSet)
api_router.register(r'api/tag', TagViewSet)
api_router.register(r'api/wiki', TagWikiViewSet)
api_router.register(r'api/match', MatchViewSet)
api_router.register(r'api/rubicore', RubicoreViewSet, base_name='rubicore')
# api_router.register(r'api/match_reason', MatchReasonViewSet)
