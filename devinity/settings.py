"""
Django settings for devinity project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""
import os
import sys
from distutils import util
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
sys.path.insert(1, os.path.dirname(os.path.realpath(__file__)))

# Quick-start development settings - unsuitable for production

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get('DEVINITY_SECRET_KEY')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = util.strtobool(os.getenv('DEVINITY_DEBUG', 'True'))
TEMPLATE_DEBUG = util.strtobool(os.getenv('DEVINITY_TEMPLATE_DEBUG', 'True'))
DEVINITY_HOSTNAME = os.getenv('DEVINITY_HOSTNAME', '127.0.0.1')

ALLOWED_HOSTS = [] if DEBUG else DEVINITY_HOSTNAME.split()

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'gunicorn',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    # ... include the providers you want to enable:
    'allauth.socialaccount.providers.google',
#    'allauth.socialaccount.providers.github',
#    'allauth.socialaccount.providers.linkedin',
#    'allauth.socialaccount.providers.linkedin_oauth2',
#    'allauth.socialaccount.providers.openid',
#    'allauth.socialaccount.providers.stackexchange',
    'rest_framework',
    'devinityapp',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'devinity.ExceptionLoggingMiddleware',
    'devinityapp.middleware.MobileDetectionMiddleware'
)

SITE_ID = os.getenv('DEVINITY_SITE_ID', '125')

MIXPANEL_KEY = os.getenv('DEVINITY_MIXPANEL_KEY')

ROOT_URLCONF = 'devinity.urls'

WSGI_APPLICATION = 'devinity.wsgi.application'


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = ''
STATIC_PATH = os.path.join(BASE_DIR + STATIC_URL)
STATICFILES_DIRS = (
    STATIC_PATH,)

# this will be general template path

TEMPLATE_DIRS = (os.path.join(BASE_DIR + STATIC_URL + 'templates'), )

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'static/templates'),
        ],

        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',

                # `allauth` needs this from django
                'django.template.context_processors.request',
            ],
        },
    },
]


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': os.environ.get('DEVINITY_DB_NAME', 'dong'),
        'USER': os.environ.get('DEVINITY_DB_USER', 'dong'),
        'PASSWORD': os.environ.get('DEVINITY_DB_PASS', 'dong'),
        'HOST': os.environ.get('DEVINITY_DB_HOST', 'dong'),
        'PORT': os.environ.get('DEVINITY_DB_PORT', '15'),
    }
}


AUTH_USER_MODEL = 'devinityapp.User'

AUTHENTICATION_BACKENDS = (
    # Needed to login by username in Django admin, regardless of `allauth`
    'django.contrib.auth.backends.ModelBackend',
    # `allauth` specific authentication methods, such as login by e-mail
    'allauth.account.auth_backends.AuthenticationBackend',

)

REST_FRAMEWORK = {
    'DEFAULT_FILTER_BACKENDS': ('rest_framework.filters.DjangoFilterBackend',),
    # 'PAGE_SIZE': 10
}

USER_IMAGE_PATH = '/home/devinity/devinity-media/user-data/'

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR + '/media/')

LOGIN_URL = '/login/'
LOGIN_REDIRECT_URL = '/signed/'
LOGOUT_URL = '/logout/'

# ALLAUTH LOGIN
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_EMAIL_VERIFICATION = 'none'  # no verification email is sent
SOCIALACCOUNT_QUERY_EMAIL = True

broker_module_dir = os.path.dirname(__file__)

# CELERY STUFF
BROKER_URL = ONCE_REDIS_URL = open(os.path.join(broker_module_dir, 'broker_address.cfg')).read()
ONCE_DEFAULT_TIMEOUT=300
BROKER_CONNECTION_MAX_RETRIES = 120 #  0 is forever
BROKER_HEARTBEAT = 10
BROKER_POOL_LIMIT = 30
CELERYD_WORKER_LOST_WAIT = 60
REDIS_CONNECT_RETRY = True
CELERYD_POOL_RESTARTS = True
BROKER_TRANSPORT_OPTIONS = {'socket_timeout': 90}

CELERY_RESULT_BACKEND = open(os.path.join(broker_module_dir, 'broker_results_address.cfg')).read()
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_REDIS_MAX_CONNECTIONS = 30
# http://stackoverflow.com/questions/13866926/python-pytz-list-of-timezones
# Because Django app is set to UTC, let's hold with setting custom timezone
# CELERY_TIMEZONE = 'Europe/Warsaw'
from datetime import timedelta
# from celery.schedules import crontab
# TODO: find out how to make crontab working
CELERY_TASK_RESULT_EXPIRES = timedelta(days=3)
blacklisted_orgs = [1]

CELERYBEAT_SCHEDULE = {
    'rubicore.integrity_watcher': {
        'task': 'rubicore.integrity_watcher',
        'schedule': timedelta(hours=12),
        'args': (blacklisted_orgs,)
    },
    'tags_manager.annotate': {
        'task': 'tags_manager.annotate',
        'schedule': timedelta(days=15),
        'args': (None,)
    }
}
