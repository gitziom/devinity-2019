class SearchSectionController {
  select = {};
  choices = {};
  search = {};
  searching = {};

  constructor(Restangular, Devinity, toastr) {
    this.Restangular = Restangular;
    this.Devinity = Devinity;
    this.toastr = toastr;
    this.getFlagChoices();
  };

  getSkillChoices(query) {
    this.getTags(query, 'skills');
  };

  getWantToLearnChoices(query) {
    this.getTags(query, 'wantToLearn');
  };

  getTags(query, choices) {
    if(query.length > 2) {
      this.searching[choices] = true;
      this.Restangular.all('tag').getList({name: query})
        .then(result => {
          this.searching[choices] = false;
          this.choices[choices] = result.plain();
        })
    } else {
      this.choices[choices] = [];
    }
  };

  getFlagChoices() {
    this.searching.flags = true;
    this.Restangular.all('flag').getList()
      .then(result => {
        this.choices.flags = _.map(result, flag => {
          switch (flag.category) {
            case 1:
              flag.categoryName = 'Opportunity';
              break;
            case 2:
              flag.categoryName = 'Information';
              break;
            case 3:
              flag.categoryName = 'Warning';
              break;
          }
          return flag
        });
        this.flags = [];
      })
  };

  doSearch() {
    this.toastr.info('', 'Searching');

  }

  static $inject = ['Restangular', 'Devinity', 'toastr']
}

export default SearchSectionController
