class SkillRankingListController {
  userCount;

  constructor(modal, Devinity, Restangular, $q) {
    this.modal = modal;
    this.Devinity = Devinity;
    this.Restangular = Restangular;
    this.ranking = [];
    this.q = $q;

    this.getRanking();
  }

  getRanking() {
    this.init = true;
    this.Restangular.all('skills/rank_owned').getList()
      .then(rank => {
        this.ranking = _.sortByOrder(rank.plain(), ['count'], ['desc']);
        this.init = false;
      })
  }

  getPercent(skill) {
    return parseInt((skill.count / this.userCount) * 100);
  }

  showInfo(skill) {
    this.Devinity.showRankingSkillInfo(skill)
  }

  static $inject = ['$uibModal', 'Devinity', 'Restangular', '$q']
}

export default SkillRankingListController;
