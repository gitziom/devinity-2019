export default class PeerMatchController {
	peer

	constructor(Restangular, toastr, $rootScope) {
		this.Restangular = Restangular
		this.toastr = toastr
		this.$rootScope = $rootScope
	}

	showSuccessMessage() {
		this.toastr.success('', 'Match reported')
	}

	showError() {
		this.toastr.error('', 'Error occurred')
	}

	static $inject = ['Restangular', 'toastr', '$rootScope']
}
