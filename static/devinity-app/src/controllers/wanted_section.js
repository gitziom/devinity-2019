/**
 * Created by Wojciech Rydel <rydel.wojciech@gmail.com> on 21.11.15.
 */

class WantedStuffController {
  constructor(Restangular, $uibModal, Devinity, toastr, $q) {
    this.edit = false;
    this.Restangular = Restangular;
    this.modal = $uibModal;
    this.Devinity = Devinity;
    this.toastr = toastr;
    this.choices = [];
    this.skills = [];
    this.tags = [];
    this.q = $q;
    this.initializing = true;

    var userId = parseInt(window.localStorage.getItem('userId'));
    var query = (userId) ? {user: userId} : {};

    this.Restangular.all('skills/wanted').getList(query)
      .then(result => {
        this.skills = result.plain();
        this.tags = _.map(this.skills, skill => skill.tag);
        this.initializing = false
      })
  }

  editSkills() {
    this.userFlagsCopy = angular.copy(this.skills);
    this.edit = true;
  }

  cancelEditing() {
    this.skills = this.userFlagsCopy;
    this.tags = _.map(this.skills, skill => skill.tag);
    this.edit = false;
  }

  saveSkills() {
    this.saving = true;
    var previousTagsIds = _.map(this.userFlagsCopy, skill => skill.tag.id);
    var currentTagsIds = _.map(this.tags, tag => tag.id);
    var tagsToDelete = _.reject(previousTagsIds, id => _.includes(currentTagsIds, id));
    var tagsToAdd = _.reject(currentTagsIds, id => _.includes(previousTagsIds, id));
    var skillsToDelete = _.filter(this.userFlagsCopy, skill => _.includes(tagsToDelete, skill.tag.id));
    var deletePromises = _.map(skillsToDelete, skill => this.Restangular.one('skills',  skill.id).remove());

    this.q.all(deletePromises).then(result => {
      this.skills = _.reject(this.skills, skill => _.includes(_.map(skillsToDelete, skill => skill.id), skill.id));
      this.q.all(_.map(tagsToAdd, tagId => this.Restangular.all('skills').post({
        tag: tagId,
        familiar: this.Devinity.familiar.wantToLearn
      })))
        .then(addedSkills => {
          _.forEach(addedSkills, skill => {
            skill.tag = _.filter(this.tags, tag => tag.id == skill.tag)[0]
          });
          this.skills = _.union(this.skills, addedSkills);
          this.edit = false;
          this.saving = false;
          this.toastr.success('', 'Updated stuff you want to learn')
        })
    });

  }

  getChoices(search) {
    if(search && search.length > 0) {
      this.searching = true;
      this.Restangular.all('tag').getList({
        name: search,
        exclude: _.map(this.tags, tag => tag.id)
      })
        .then(result => {
          this.choices = result.plain();
          this.searching = false
        })
    } else {
      this.choices = []
    }
  }

  showInfo(skill) {
    this.Devinity.showWantedSkillInfo(skill);
  }

  static $inject = ['Restangular', '$uibModal', 'Devinity', 'toastr', '$q']
}

export default WantedStuffController
