import {DEFAULT_AVATAR} from './../constants'

class MatchLoader {
  $q;
  resource;

  constructor(Restangular, $q, $scope, path, title, {homo=false, learn=false}={}) {
    this.$q = $q;
    this.homo = homo;
    this.learn = learn;
    this.resource = Restangular.all(path);
    this.title = title;
    this.matches = [];
    this.hidden = true;
    this.loaded = false;
    this.loading = false;
    this.from = 1;
    this.to = 10;

    $scope.$on('match-reported', this.removeReportedMatch.bind(this))
  }

  removeReportedMatch(event, peerId) {
    this.matches = _.reject(this.matches, match => match.id == peerId)
  }

  next() {
    if (!this.loaded) {
      this._loadPage()
      .then(page => {
        let others = _.map(page, match => {
          let score = this.homo ? (match.homophily_strength * 100) : 
            (match.heterophily_strength * 100)
         
          match.score = score.toFixed(2)
          match.user = this.learn ? match.user_1 : match.user_2
          match.user.image = match.user.avatar || DEFAULT_AVATAR

          return match;
        });

        this.matches = this.matches.concat(others)
        this.from += 10
        this.to += 10
      })
    }
  }

  clear() {
    this.from = 1
    this.to = 10
    this.matches = []
    this.loaded = false
  }

  _loadPage() {
    let deferred = this.$q.defer();
    let query = {from: this.from, to: this.to};
    var userId = parseInt(window.localStorage.getItem('userId'));
    if (userId) query.userid = userId;
    this.loading = true;

    this.resource.getList(query)
      .then(result => {
        let matchCount = result.length;

        this.loading = false;
        result = matchCount ? result.plain() : [];
        this.loaded = !matchCount || !(matchCount < 10);
        deferred.resolve(result)
      })
      .catch(error => {
        this.loading = false;
        deferred.reject(error)
      });
    return deferred.promise
  }
}

class MatchingSectionController {
  constructor(Restangular, Devinity, $q, $scope) {
    this.Restangular = Restangular;
    this.Devinity = Devinity;
    this.q = $q;
    this.$scope = $scope
    this.list = [true, true, true];

    this.getPeers();

    $scope.$on('match-reported', this.removeReportedMatch.bind(this))
  }

  removeReportedMatch(event, peerId) {
    this.matches = _.reject(this.matches, match => match.id == peerId)
  }

  showList(others) {
    if (others.hidden) {
      others.next();
      others.hidden = false;
    } else {
      others.hidden = true;
      others.clear();
    }
  }

  getPeers() {
    this.init = true;

    var userId = parseInt(window.localStorage.getItem('userId'));
    var query = (userId) ? {userid: userId} : {};

    this.Restangular.all('match/matches_all').getList(query)
      .then(result => {
        result = result.length ? result.plain() : [];
        this.matches = _.map(result, (match, index) => {
          let obj = _.extend({}, match);
          obj.user = obj.user_2
          obj.user.image = obj.user.avatar || DEFAULT_AVATAR
          obj.score = (index == 0) ? 
            (obj.homophily_strength * 100).toFixed(2) 
              : 
            (obj.heterophily_strength * 100).toFixed(2)

          switch (obj.match_type) {
            case 'similar':
              obj.others = new MatchLoader(this.Restangular,this.q, this.$scope, 'match/similar', 'PEERS LIKE YOU', {homo: true})
              break
            case 'student':
              obj.others = new MatchLoader(this.Restangular, this.q, this.$scope, 'match/students', 'PEERS YOU CAN LEARN FROM')
              break
            case 'teacher':
              obj.others = new MatchLoader(this.Restangular, this.q, this.$scope, 'match/teachers', 'PEERS YOU CAN TEACH')
              break
          }

          return obj
        });
        this.init = false;
      })
  }

  static $inject = ['Restangular', 'Devinity', '$q', '$scope']
}

export default MatchingSectionController
