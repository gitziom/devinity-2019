/**
 * Created by Wojciech Rydel <rydel.wojciech@gmail.com> on 21.11.15.
 */

class FlagRankingController {
  modal;
  flags;
  urls = {flags: 'flag'};
  Restangular;
  Devinity;

  constructor($uibModal, Restangular, Devinity) {
    this.modal = $uibModal;
    this.Restangular = Restangular;
    this.Devinity = Devinity;
    this.getFlagRanking()
  }

  getFlagRanking() {
    this.flags = {opportunity: [], warning: [], information: []};
    this.initializing = true;
    this.Restangular.all(this.urls.flags).getList()
      .then(flags => {
        this.flags.opportunity = _.filter(flags, flag => flag.category == 1);
        this.flags.warning = _.filter(flags, flag => flag.category == 3);
        this.flags.information = _.filter(flags, flag => flag.category == 2);
        this.initializing = false;
      })
  };

  showInfo(flag) {
    this.Devinity.showFlagInfo(flag);
  }

  countInformationFlags() {
    return this.countFlags('information');
  };

  countOpportunityFlags() {
    return this.countFlags('opportunity');
  };

  countWarningFlags() {
    return this.countFlags('warning');
  }

  countFlags(type) {
    var counter = 0;
    _.each(this.flags[type], flag => {
      counter += flag.count;
    });
    return counter;
  }

  static $inject = ['$uibModal', 'Restangular', 'Devinity'];
}

export default FlagRankingController
