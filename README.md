[![Build Status](https://drone.io/bitbucket.org/devinityteam/devinity/status.png)](https://drone.io/bitbucket.org/devinityteam/devinity/latest)

# README #

This is Devinity main system development project.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

#### Summary of set up
Application consists of two main components: web frontend and standalone web service. First one is django website, while the other is standalone python. Communication between both is based on REST 

#### Configuration

After cloning this repo you may wish to run it locally for testing. It will be necessary to have the following installed on your machine:

* Python
* nginx (or other web server, but if you look for 1:1 representation of target configuration nginx should be the choice)
* packages used by application, you may get it by running following command in your repo directory
```
#!shell
pip install -r requirements.txt
```
-------------------
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

#### Writing tests

Write unit test. Doctest is useful as well. More information about testing your code: [Python guide - "Testing Your Code"](http://docs.python-guide.org/en/latest/writing/tests/)

#### Code review

Implement a code-review system. At the moment, at least make a Hangout chat to show your code before pull-request, preferably at the end of `EOB`.

#### Resolving conflicts

You may find it useful: https://confluence.atlassian.com/bitbucket/resolve-merge-conflicts-704414003.html

#### Other guidelines

Configure automatic `PEP8` checks. All submitted code must be PEP8, it's worthy to mention that some tools like `autopep8` make automatic checks and corrections of code (syntax, naming, etc.).

### Who do I talk to? ###

#### Repo owner or admin

**Marcin Gębski** or Oskar Jarczyk

#### Other community or team contact

Devinity **@ Slack**