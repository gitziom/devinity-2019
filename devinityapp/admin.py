from django.contrib import admin

# Register your models here.

from . import models

admin.site.register(models.Organization)
admin.site.register(models.Social_SO)
admin.site.register(models.Social_GH)
admin.site.register(models.User)
admin.site.register(models.ExpertProfile)
admin.site.register(models.Tag)
admin.site.register(models.Skill)
admin.site.register(models.Flag)
admin.site.register(models.FlagSet)
