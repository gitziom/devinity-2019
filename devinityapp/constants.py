from collections import namedtuple
import math


def productivity_level(knowledge, c = 6):
	return 1 / (1 + (math.e ** (- (-c + (2*c*knowledge) ) ) ) )


Level = namedtuple('Level', ['level', 'productivity'])
Shu = Level(0.33, productivity_level(0.33))
Ha = Level(0.66, productivity_level(0.66))
Ri = Level(0.95, productivity_level(0.95))

advance = 0.3
