# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('devinityapp', '0014_auto_20151119_2002'),
    ]

    operations = [
        migrations.AlterField(
            model_name='skill',
            name='importance',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='skill',
            name='level',
            field=models.DecimalField(blank=True, null=True, max_digits=10, decimal_places=10, choices=[(0.880797077, b'Know it'), (0.119202922, b'Can work with it'), (0.997527376, b'Can teach it')]),
        ),
    ]
