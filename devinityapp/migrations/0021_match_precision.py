# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('devinityapp', '0020_match_reasons_tags'),
    ]

    operations = [
        migrations.AlterField(
            model_name='match',
            name='heterophily_strength',
            field=models.DecimalField(max_digits=12, decimal_places=11),
        ),
        migrations.AlterField(
            model_name='match',
            name='homophily_strength',
            field=models.DecimalField(max_digits=12, decimal_places=11),
        ),
    ]
