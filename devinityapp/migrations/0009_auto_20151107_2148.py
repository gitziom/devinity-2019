# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('devinityapp', '0008_auto_20151105_1859'),
    ]

    operations = [
        migrations.AddField(
            model_name='skill',
            name='tag',
            field=models.ForeignKey(default=None, blank=True, to='devinityapp.Tag', null=True),
        ),
        migrations.AddField(
            model_name='skill',
            name='user',
            field=models.ForeignKey(default=None, blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AlterField(
            model_name='flag',
            name='category',
            field=models.PositiveSmallIntegerField(choices=[(1, b'Opportunity'), (2, b'Information'), (3, b'Warning')]),
        ),
    ]
