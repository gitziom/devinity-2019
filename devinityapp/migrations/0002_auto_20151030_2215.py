# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devinityapp', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='flagset',
            name='flag',
        ),
        migrations.RemoveField(
            model_name='flagset',
            name='user',
        ),
        migrations.DeleteModel(
            name='Flag',
        ),
        migrations.DeleteModel(
            name='FlagSet',
        ),
    ]
