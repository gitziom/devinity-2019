# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('devinityapp', '0021_match_precision'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='avatar_thumbnail',
            field=models.ImageField(null=True, upload_to=b'avatar', blank=True),
        ),
        migrations.AlterField(
            model_name='skill',
            name='level',
            field=models.DecimalField(blank=True, null=True, max_digits=10, decimal_places=10, choices=[(0.33, b'Know it'), (0.66, b'Can work with it'), (0.95, b'Can teach it')]),
        ),
        migrations.AlterField(
            model_name='user',
            name='avatar',
            field=models.ImageField(null=True, upload_to=b'avatar', blank=True),
        ),
    ]

