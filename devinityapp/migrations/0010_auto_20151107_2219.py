# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devinityapp', '0009_auto_20151107_2148'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='skill',
            name='holder',
        ),
        migrations.RemoveField(
            model_name='skill',
            name='tag_name',
        ),
    ]
