# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devinityapp', '0006_auto_20151102_1948'),
    ]

    operations = [
        migrations.CreateModel(
            name='TagWiki',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.TextField()),
            ],
        ),
        migrations.RemoveField(
            model_name='tag',
            name='count',
        ),
        migrations.RemoveField(
            model_name='tag',
            name='name',
        ),
    ]
