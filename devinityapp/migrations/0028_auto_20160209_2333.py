# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('devinityapp', '0027_feedback_reporter'),
    ]

    operations = [
        migrations.AddField(
            model_name='organization',
            name='manager_key',
            field=models.CharField(default=None, max_length=20),
        ),
        migrations.AddField(
            model_name='organization',
            name='user_key',
            field=models.CharField(default=None, max_length=20),
        ),
        migrations.AlterField(
            model_name='organization',
            name='description',
            field=models.TextField(blank=True),
        ),
    ]
