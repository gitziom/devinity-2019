# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('devinityapp', '0019_updated_match_reasons'),
    ]

    operations = [
        migrations.RenameField(
            model_name='match',
            old_name='reasons',
            new_name='reason',
        ),
        migrations.AlterField(
            model_name='matchreason',
            name='left_teaches_right',
            field=models.ManyToManyField(default=None, related_name='matchreason_left_mastery', null=True, to='devinityapp.Tag', blank=True),
        ),
        migrations.AlterField(
            model_name='matchreason',
            name='right_teaches_left',
            field=models.ManyToManyField(default=None, related_name='matchreason_right_mastery', null=True, to='devinityapp.Tag', blank=True),
        ),
        migrations.AlterField(
            model_name='matchreason',
            name='shared_skills',
            field=models.ManyToManyField(default=None, to='devinityapp.Tag'),
        ),
    ]
