# -*- coding: utf-8 -*-
import os
from threading import Thread

from Queue import Queue
import time
import datetime

DB_HOST = 'octocat.c7h1kidt7oge.eu-west-1.rds.amazonaws.com'
DB_PORT = 3306
DB_NAME = 'stack'

ALLTAGS = 'SELECT id,tagname FROM tags'
ALLRELATIONS = 'SELECT id, tag1, tag2, count FROM tag_relations'

processed = 0
starttime = datetime.datetime.now()

import MySQLdb as mdb
import stackexchange
from stackexchange import StackExchangeError

# caches
global relation_cache
relation_cache = ()
global tag_ids
tag_ids = dict()

# counters
global processedcnt
processedcnt= 0
global relatedcnt
relatedcnt = 0
global all_relations
all_relations = 0
global createdcnt
createdcnt = 0
global all_created
all_created = 0

q = Queue()
se_api_key = os.environ.get('DEVINITY_API_SE', '')
so = stackexchange.Site(stackexchange.StackOverflow, se_api_key)
so.impose_throttling = True
so.throttle_stop = False
usr = os.environ.get('DEVINITY_DB2_USER', '')
pwd = os.environ.get('DEVINITY_DB2_PASS', '')
con = mdb.connect(host=DB_HOST, user=usr, passwd=pwd, db=DB_NAME,
                  connect_timeout=50000000, charset='utf8',
                  init_command='SET NAMES UTF8', use_unicode=True)
def determine_wait(str, time=0):
    import re
    m = re.match(".+available in (.+) seconds$", str)
    if m:
        sub = m.group(1)
        time = int(sub)
    return time


# relation has no direction, and is stored once in both DB and this cache
# if relation A to B is stored, then relation of B to A is not allowed to be added
def relation_exists(id1, id2):
    try:
        for item in relation_cache:
            if (item[1] == id1 and item[2] == id2) or \
                    (item[1] == id2 and item[2] == id1):
                return True
    except e:
        print 'relation check failed terribly: ', e.message
        pass
    else:
        return False

def insert(bulk, conn):
    with conn:
        sql = 'INSERT INTO tag_relations (tag1, tag2, count, updated) VALUES '
        for item in bulk:
            sql += '(%d, %d, %d, NOW()),' % (item[0][1], item[0][2], item[0][3])
        sql = sql[:-1] + ';'

        ins = conn.cursor()
        ins.execute(sql)
        conn.commit()
        ins.close()


def insert_records(number=18):
    usr = os.environ.get('DEVINITY_DB2_USER', '')
    pwd = os.environ.get('DEVINITY_DB2_PASS', '')
    con2 = mdb.connect(host=DB_HOST, user=usr, passwd=pwd, db=DB_NAME,
                       connect_timeout=50000000, charset='utf8',
                       init_command='SET NAMES UTF8', use_unicode=True)

    ceiling = number
    bulk = []
    i = 0
    lag = 0

    while i < ceiling +1:
        if i == ceiling:
            insert(bulk, con2)
            bulk = []
            i = 0
        else:
            # adjust throttle
            '''if (q.qsize() >= 3 * ceiling):
                lag += 1
                if lag > 2:
                    ceiling += 1
                    lag
            else:
                if ceiling > number:
                    ceiling -= 1'''
            item = q.get()
            if item:
                if item[0] == -2:
                    break # i = number + 1 # end of loop processing
                else:
                    bulk.append(item)
                    i += 1
                    #q.task_done()

    # insert outstanding items
    insert(bulk, con2)
    q.task_done()
    con2.close()


def add_new_tag(tag):
    attempt = True
    while attempt:
        try:
            t = so.tag(tag)
        except StackExchangeError, e:
            print "SE Error: " + e.message
            if e.code == 502:
                if e.name == 'throttle_violation':
                    seconds = determine_wait(e.message)
                    print 'Going to sleep for %s seconds' % seconds
                    time.sleep(seconds)
                else:
                    time.sleep(10)  #some unknown error
                    pass
        else:
            attempt = False
    #update db
    with con:
        # id | tagname | tagcount
        qry = 'INSERT INTO tags (tagname, tagcount) VALUES '
        qry += '("%s", %s);' % (t.json['name'], t.json['count'])
        cur = con.cursor()
        cur.execute(qry)
        con.commit()
        #update cache
        tag_ids[t.json['name']] = cur.lastrowid
        cur.close()
        print 'Added new tag: %s ' % t.json['name'],
        return tag_ids[t.json['name']]


def add_new_relation(row):
    global relation_cache
    global createdcnt
    relation_cache += row
    # -1 at first position indicates that this row hasn't been synchronized to DB yet
    q.put(row)
    createdcnt += 1
    #q.join()

def process_relation(relations, id1):
    cnt = 0
    for relation in relations:
        if not tag_ids.has_key(relation.json['name']):
            id2 = add_new_tag(relation.json['name'])
            if id2 == 0:
                continue # error in tag handling, skip it
        else:
            id2 = tag_ids[relation.json['name']]

        if not relation_exists(id1, id2):
            item = ( (long(-1), long(id1), long(id2), long(relation.json['count'])), )
            add_new_relation(item)
            cnt += 1
    return cnt

def last_added_relation():
    max = 0
    for row in relation_cache:
        if row[1] > max:
            max = row[1]
    return max

with con:
    print 'Quering DB for all tags... ',
    master_tag = con.cursor()
    master_tag.execute(ALLTAGS)
    tags = master_tag.fetchall()
    print 'Found %s tags' % len(tags)

    print 'Building inversed lookup... ',
    for row in tags:
        # id | tagname -> tagname : id
        tag_ids[row[1]] = row[0]
    print 'Inverted %s entries' % len(tag_ids)

    print 'Quering DB for existing relations... ',
    master_relation = con.cursor()
    master_relation.execute(ALLRELATIONS)
    relation_cache = master_relation.fetchall()
    print 'Found %s relations' % len(relation_cache)

    t = Thread(target=insert_records)
    t.start()

    last_processed = last_added_relation()

    print'Processing...'
    for row in tags:
        # tagname : id
        tagname = row[1]
        print ('%s: %s' % (tag_ids[tagname], tagname)),

        if tag_ids[tagname] < last_processed:
            print 'relations present in DB, skipping...'
            continue

        st = 'attempt'
        while st != 'done':
            if st == 'processing':
                # process relation here
                relatedcnt += len(related)
                createdcnt += process_relation(related, tag_ids[tagname])
                try:
                    if not related.fetch_next():
                        print 'fetched %s relations, added %s new' % (relatedcnt, createdcnt)
                        all_relations = relatedcnt
                        all_created = createdcnt
                        relatedcnt = 0
                        createdcnt = 0
                        st = 'done'
                except StackExchangeError, e:
                    print "SE Error: " + e.message
                    if e.code == 502:
                        if e.name == 'throttle_violation':
                            seconds = determine_wait(e.message)
                            print 'Going to sleep for %s seconds, starting on %s' % (seconds, datetime.datetime.now())
                            time.sleep(seconds+1)
                        else:
                            time.sleep(10) # Some unknown error, wait 10 seconds before retrying. Just in case,
            elif st == 'attempt':
                try:
                    time.sleep(1)
                    related = so.tag_related(tagname, page=1, pagesize=60)
                except StackExchangeError, e:
                    print "SE Error: " + e.message
                    if e.code == 502:
                        if e.name == 'throttle_violation':
                            seconds = determine_wait(e.message)
                            print 'Going to sleep for %s seconds, starting on %s' % (seconds, datetime.datetime.now())
                            time.sleep(seconds+1)
                        else:
                            time.sleep(10) # Some unknown error, wait 10 seconds before retrying. Just in case,
                else:
                    st = 'processing'

    q.put([-2]) # thread termination signal
    q.join()
    print 'Processed %s relations between %s tags and added %s new to database' % (
    all_relations, len(tag_ids), all_created)
    print 'Processing time: ', (datetime.datetime.now() - starttime).total_seconds()
    print "SE quota left: ", so.requests_left

    master_tag.close()
    master_relation.close()
    con.close()

