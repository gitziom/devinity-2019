# -*- coding: utf-8 -*-
import os
from threading import Thread
from Queue import Queue
import MySQLdb as mdb
import stackexchange
import time
import datetime

DB_HOST = 'octocat.c7h1kidt7oge.eu-west-1.rds.amazonaws.com'
DB_PORT = 3306
DB_NAME = 'stack'


class SEProvider():

    api_result = dict()

    def __init__(self):
        self.se_api_key = os.environ.get('DEVINITY_API_SE', '')
        self.so = stackexchange.Site(stackexchange.StackOverflow, self.se_api_key)
        self.so.impose_throttling = True
        self.so.throttle_stop = False
        #init DB
        usr = os.environ.get('DEVINITY_DB2_USER', '')
        pwd = os.environ.get('DEVINITY_DB2_PASS', '')
        self.db_conn = mdb.connect(host=DB_HOST, user=usr, passwd=pwd, db=DB_NAME,
                                  connect_timeout=50000000, charset='utf8',
                                  init_command='SET NAMES UTF8', use_unicode=True)

    def execute_api_call(self, kind, pattern, immediate=False, func=None):
        """
        executes call by use of SE API, supporting quota excessions
        :param kind: one of "tag" "wiki" or "related" keywords
        :param pattern: pattern or keyword to match exactly
        :param immediate: if True then will return first available result, or collect entire output otherwise (default)
        :param func: function to perform on collected data
        :return:
        """
        queue = Queue()
        qry = SEApiQuery(self.so, self.db_conn)
        t = Thread(target=qry.execute_async_api_call, args=[kind, pattern, func, queue],)
        t.start()
        result = ()
        while True:
            if not queue.empty():
                part = queue.get()
                if part == -2:
                    break
                else:
                    #result.join(part)
                    result += part

                if immediate:
                    break
                else:
                    time.sleep(1)

                #part.join(part)
        return result

    def get_tag(self, pattern, func=None, **kwargs):
        return self.execute_api_call('tag', pattern, func)

    def get_tag_wiki(self, pattern, func=None, **kwargs):
        return self.execute_api_call('wiki', pattern, False, func)

    def get_tag_related(self, pattern, func=None, **kwargs):
        return self.execute_api_call('related', pattern, func)


class SEApiQuery():
    api_result = dict()

    def __init__(self, so_, db_):
        self.so = so_
        self.db = db_

    @staticmethod
    def determine_wait(response_str, wait_time=0):
        import re
        m = re.match(".+available in (.+) seconds$", response_str)
        if m:
            sub = m.group(1)
            wait_time = int(sub)
        return wait_time

    def handle_exceeded_quota(self, e):
        print "SE Error: " + e.message
        if e.code == 502:
            if e.name == 'throttle_violation':
                seconds = self.determine_wait(e.message)
                print 'Going to sleep for %s seconds, starting on %s' % (seconds, datetime.datetime.now())
                time.sleep(seconds + 1)
            else:
                time.sleep(10) # Some unknown error, wait 10 seconds before retrying. Just in case,

    def execute_async_api_call(self, kind, pattern, func=None, queue=None, **kwargs):
        collected = 0
        processed = 0
        st = 'attempt'
        while st != 'done':
            #
            if st == 'processing':  # process and query more
                # process relation here
                collected += len(api_result)
                if func is not None:
                    processed += func(api_result)

                try:
                    if not api_result.fetch_next():
                        st = 'done'
                except stackexchange.StackExchangeError, e:
                    self.handle_exceeded_quota(e)
                else:
                    pass

            elif st == 'attempt': # API query
                try:
                    time.sleep(1)
                    if kind == 'tag':
                        api_result = self.so.tag(pattern, page=1, pagesize=60)
                    elif kind == 'wiki':
                        api_result = self.so.tag_wiki(pattern, page=1, pagesize=60)
                    elif kind == 'related':
                        api_result = self.so.tag_related(pattern, page=1, pagesize=60)
                except stackexchange.StackExchangeError, e:
                    self.handle_exceeded_quota(e)
                else:
                    if queue is not None:
                        queue.put(api_result)
                    st = 'processing'

        print '%s:%s fetched %s of and stored %s new' % (kind, pattern, collected, processed)
        if queue is not None:
            queue.put(-2)
