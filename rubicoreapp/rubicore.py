from flask_restful import Resource
from database import session
from models import Skill, User


class Rubicore():
	def __init__(self):
		print "Rubicore object created"

	def collect_all(self):
		print "Getting all objects from SQLAlchemy"
		skillset = session.query(Skill).join(User, Skill.user_id == User.id).all()
		return skillset

	def collect(self, organization_id):
		print "Getting all objects from SQLAlchemy"
		# join Skill with Users to filter by organization
		skillset = session.query(Skill).join(User, Skill.user_id == User.id).filter(User.organization_id == organization_id).all()
		return skillset

	def intelligence(self, organization=None):
		data = self.collect(organization) if organization is not None else self.collect_all()


class MjolnirAll(Resource):
    def get(self):
        print "Mjolnir all skills."
        Rubicore().intelligence()
        return {'status': 'calculating all'}

# dodaj parametr z id organizacji!
class Mjolnir(Resource):
    def get(self, organization_id):
        print "Mjolnir skills from organization " + str(organization_id)
        Rubicore().intelligence(organization_id)
        return {'status': 'calculating'}
