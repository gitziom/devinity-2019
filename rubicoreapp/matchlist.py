from flask_restful import reqparse, abort, Resource


matches = dict()
parser = reqparse.RequestParser()
parser.add_argument('user1', required=True)
parser.add_argument('user2')
parser.add_argument('score')
parser.add_argument('human_response')


def abort_if_match_doesnt_exist(match_id):
    if match_id not in matches:
        abort(404, message="Match {} doesn't exist".format(match_id))


class Match(Resource):
    def get(self, match_id):
        abort_if_match_doesnt_exist(match_id)
        return matches[match_id]

    # def delete(self, match_id):
    #     abort_if_match_doesnt_exist(match_id)
    #     del matches[match_id]
    #     return '', 204

    # at the moment no functionaliry requires deleting
    # for algorithm tuning we simply
    # use pust/post with human_response attribute

    def put(self, match_id):
        args = parser.parse_args()
        match = {'user1': args['user1'], 'user2': args['user2'],
                 'human_response': args['human_response']}
        matches[match_id] = match
        return match, 201


class MatchList(Resource):
    def get(self):
        return matches

    def post(self):
        args = parser.parse_args()
        match_id = max(matches.keys()) + 1 if len(matches.keys()) > 0 else 1
        matches[match_id] = {'user1': args['user1'], 'user2': args['user2'],
                             'human_response': args['human_response']}
        return matches[match_id], 201
