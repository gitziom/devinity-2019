from celery.decorators import task
from celery.utils.log import get_task_logger
import time
from django.apps import apps
import yaml
import urllib2


linguist_url = 'https://raw.githubusercontent.com/github/linguist/master/lib/linguist/languages.yml'


def load_from_app_registry():
	return (apps.get_model('devinityapp', 'Tag'),
		   apps.get_model('devinityapp', 'TagCategory'))


def get_linguist_data():
	return urllib2.urlopen(linguist_url).read()


def get_unknown_category(tag_category_factory):
	return tag_category_factory.objects.get(id=1)


def get_or_create_category(tag_category_factory, category_name):
	if category_name == 'programming':
		category_name = 'programming language'
	obj, created = tag_category_factory.objects.get_or_create(description=category_name)
	return obj


@task(name='tags_manager.annotate')
def annotate(dummy):
	TagAnnotator().intelligence()


class TagAnnotator():

	def __init__(self, source='linguist'):
		self.source = source

	def intelligence(self):
		print "Starting Tag annotation (source is " + self.source + ")"

		self.start = time.clock()

		TagModel, TagCategoryModel = load_from_app_registry()
		self.tags = TagModel.objects.all()
		self.categories = TagCategoryModel.objects.all()

		self.languages = yaml.load(get_linguist_data())
		self.languages_lower = {k.lower():v for k,v in self.languages.items()}

		for tag in self.tags:
			name = tag.name.lower()
			if name in self.languages_lower:
				category_name = self.languages_lower[name]['type']
				tag.category = get_or_create_category(TagCategoryModel, category_name)
				tag.save()
			else:
				tag.category = get_unknown_category(TagCategoryModel)
				tag.save()

		self.end = time.clock()
		print "TagAnnotator execution time was {0:.4f} seconds".format(self.end - self.start)
