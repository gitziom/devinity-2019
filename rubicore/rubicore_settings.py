from enum import Enum, unique
from ast import literal_eval as make_tuple
import ConfigParser, os


filename = 'rubicore_settings.cfg'
module_dir = os.path.dirname(__file__)
file_path = os.path.join(module_dir, filename)


@unique
class RubicoreVersion(Enum):
	#basic = 1
	rubi = 2
	fudgesicle = 3


def read_settings(opened_file):
	config = ConfigParser.ConfigParser()
	config.readfp(opened_file)
	return config


def get_version(return_enum=False):
	with open(file_path) as opened_file:
		version = read_settings(opened_file).getint('Rubicore version', 'current')
	return RubicoreVersion(version) if return_enum else version


def get_available_version():
	with open(file_path) as opened_file:
		available = read_settings(opened_file).get('Rubicore version', 'available')
	return make_tuple(available)


def generate_version_list():
	return [member.value for name, member in RubicoreVersion.__members__.items()]


def generate_version_tuple():
	return tuple(member.value for name, member in RubicoreVersion.__members__.items())


def set_version(chosen_version):
	with open(file_path, 'wb') as opened_file:
		modified = ConfigParser.ConfigParser()
		modified.add_section('Rubicore version')
		modified.set('Rubicore version', 'current', chosen_version)
		modified.set('Rubicore version', 'available', generate_version_tuple())
		modified.write(opened_file)


def is_legit_version(version_number):
	return (version_number in get_available_version()) and (version_number in generate_version_list())
