from django.apps import apps


def load_from_registry(classes=['Skill', 'Match', 'MatchReason']):
    return tuple(apps.get_model('devinityapp', c) for c in classes)


def load_users_from_registry():
    return apps.get_model('devinityapp', 'User')


def load_organizations_from_registry():
    return apps.get_model('devinityapp', 'Organization')
