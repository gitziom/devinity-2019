import itertools
from django.db import transaction
import rubicore.rubicore_tools as rb
import time
import rubicore.devinity_connector as apps
import rubicore.transactional_connector as rubi_transactional
from devinityapp import constants as shuhari

__version__ = '1.2'  # Rubicore "fudgesicle"


class Fudgesicle():
    def __init__(self, organization=None):
        self.organization = organization

    def collect(self, skills):
        dataset = skills.objects
        if self.organization is None:
            return dataset
        if type(self.organization) == int:
            return dataset.filter(user__organization__id=self.organization)
        else:
            return dataset.filter(user__organization=self.organization)

    def calculate_left_eq_homophily(self, shared_tags, skills, user, filter_type):
        common_programming_lang = [shuhari.productivity_level(float(s.level)) for s in
                                   rb.filter_user_programming_skills(skills, user, filter_type) \
                                   if (s.tag in shared_tags) and (s.level is not None)]
        return max(common_programming_lang) if len(common_programming_lang) > 0 else 0

    def calculate_right_eq_homophily(self, shared_tags, skills, user, filter_type):
        return float(len(shared_tags)) / rb.count_user_skills(skills, user, filter_type)

    def calculate_homophily(self, shared_tags, skills, user, filter_type):
        return (0.8 * self.calculate_left_eq_homophily(shared_tags, skills, user, filter_type)) \
               + (0.2 * self.calculate_right_eq_homophily(shared_tags, skills, user, filter_type))

    # Pareto principle, 80% of software engineering job is done by programming languages
    # 20% of score consists of evaluating intersection of all skills

    def calculate_left_eq_heterophily(self, different_tags, skills, user, filter_type):
        different_programming_languages = [shuhari.productivity_level(float(s.level)) for s in
                                           rb.filter_user_skills(skills, user, filter_type) \
                                           if (s.tag in different_tags) and (s.level is not None)]
        return max(different_programming_languages) if len(different_programming_languages) > 0 else 0

    def calculate_right_eq_heterophily(self, different_tags, skills, user, filter_type):
        return float(len(different_tags)) / rb.count_user_skills(skills, user, filter_type)

    def calculate_heterophily(self, different_tags, skills, right_user, left_user, filter_type, opposite_type):
        return (0.8 * self.calculate_left_eq_heterophily(different_tags, skills, left_user, opposite_type)) \
               + (0.2 * self.calculate_right_eq_heterophily(different_tags, skills, right_user, filter_type))

    def intelligence(self, filter_by_user=None):
        print "Starting Fudgesicle" + (" full run" if filter_by_user is None else "")

        self.start = time.clock()

        SkillModel, MatchModel, MatchReasonModel = apps.load_from_registry()

        if (filter_by_user is not None) and (type(filter_by_user) == int):
            filter_by_user = apps.load_users_from_registry().objects.get(id=filter_by_user)

        self.mastership_type = (SkillModel.KNOWS, SkillModel.PASSIONATE)
        self.student_type = (SkillModel.WANT_TO_LEARN,)

        self.queryset = self.collect(SkillModel)
        self.skills = self.queryset.all()
        self.users = set([skill.user for skill in self.skills])
        self.pairs = itertools.combinations(self.users, 2)
        self.processed = 0

        for pair in self.pairs:

            if (filter_by_user is not None) and (filter_by_user not in pair):
                continue

            left_user, right_user = pair

            pair_shared_tags = rb.intersect_tags(self.skills, left_user, right_user,
                                                 allowed_skill_types=self.mastership_type)
            # allowed_skill_types in rb.intersect_tags solves bug #110671128
            pair_different_tags = rb.mentoring_relation_tags(self.skills, left_user, right_user,
                                                             left_skill_types=self.mastership_type,
                                                             right_skill_types=self.student_type)
            # left learns from right (right teaches left)
            pair_different_tags_reversed = rb.mentoring_relation_tags(self.skills, right_user, left_user,
                                                                      left_skill_types=self.student_type,
                                                                      right_skill_types=self.mastership_type)
            # right learns from left (left teaches right)
            # print pair_different_tags == pair_different_tags_reversed

            try:
                homophily_strength = self.calculate_homophily(pair_shared_tags, self.skills,
                                                              left_user,
                                                              self.mastership_type)  # Is always between 0.0 and 1.0
            except ZeroDivisionError:
                homophily_strength = 0.0
            try:
                heterophily_strength = self.calculate_heterophily(pair_different_tags, self.skills,
                                                                  right_user=right_user, left_user=left_user,
                                                                  filter_type=self.student_type,
                                                                  opposite_type=self.mastership_type)  # Is always between 0.0 and 1.0
            except ZeroDivisionError:
                heterophily_strength = 0.0  # I'm not sure about this, could be 1.0

            match_reason = MatchReasonModel.objects.create()
            for tag in pair_shared_tags:
                match_reason.shared_skills.add(tag)
            for tag in pair_different_tags:
                match_reason.left_teaches_right.add(tag)
            for tag in pair_different_tags_reversed:
                match_reason.right_teaches_left.add(tag)
            match_reason.save()

            with transaction.atomic():
                rubi_transactional.create_or_update_match(MatchModel, left_user, right_user, homophily_strength, heterophily_strength, match_reason)

            pair_different_tags = rb.mentoring_relation_tags(self.skills, right_user, left_user,
                                                             left_skill_types=self.mastership_type,
                                                             right_skill_types=self.student_type)
            pair_different_tags_reversed = rb.mentoring_relation_tags(self.skills, left_user, right_user,
                                                                      left_skill_types=self.student_type,
                                                                      right_skill_types=self.mastership_type)

            try:
                homophily_strength = self.calculate_homophily(pair_shared_tags, self.skills, right_user,
                                                              self.mastership_type)
            except ZeroDivisionError:
                homophily_strength = 0.0
            try:
                heterophily_strength = self.calculate_heterophily(pair_different_tags, self.skills,
                                                                  right_user=left_user,
                                                                  left_user=right_user, filter_type=self.student_type,
                                                                  opposite_type=self.mastership_type)
            except ZeroDivisionError:
                heterophily_strength = 0.0

            mirror_match_reason = MatchReasonModel.objects.create()
            for tag in pair_shared_tags:
                mirror_match_reason.shared_skills.add(tag)
            for tag in pair_different_tags:
                mirror_match_reason.left_teaches_right.add(tag)
            for tag in pair_different_tags_reversed:
                mirror_match_reason.right_teaches_left.add(tag)
            mirror_match_reason.save()

            with transaction.atomic():
                rubi_transactional.create_or_update_match(MatchModel, right_user, left_user, homophily_strength, heterophily_strength, mirror_match_reason)

            self.processed += 1

        self.end = time.clock()
        print "Fudgesicle execution time was {0:.4f} seconds".format(self.end - self.start)

        return rb.result_json('{0:.5f} seconds'.format(self.end - self.start), self.processed, __version__)
