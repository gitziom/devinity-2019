#!/bin/bash

APPNAME=devinity
APPDIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

LOGFILE=$APPDIR'/log/gunicorn.log'
LOGCELERY=$APPDIR'/log/celery.log'
ERRORFILE=$APPDIR'/log/gunicorn-error.log'

cd $APPDIR

echo "[Devinity] Killing possible running instances from $APPDIR"
ps -fea | grep $APPDIR | grep -v grep | awk '{ printf $2" "}' | xargs kill -15
sleep 2

current_directory=${PWD##*/}

source /opt/ec2/devinityenv.sh
source /opt/ec2/devinitymsg.sh
source $APPDIR/devinitylocal.sh

if [ "$current_directory" == "master" ]
then
	echo "[Devinity] This is presentation branch."
	# celery --app=devinity.celery:app worker --loglevel=INFO -B --concurrency=1 --logfile=$LOGCELERY &
	# don't use --autoreload, it's buggy, besides deploybot restarts stack by running start_gunicorn.sh
	echo "[Devinity] Check if celery is already running"
	pgrep supervisord && supervisorctl restart celeryd-master
	echo "[Supervisord] Starting celery with supervisor process...."
	supervisord &>log/supervisord.log
	echo "[Supervisord] Use supervisorctl for your convenience"
	# echo "Done"
else
	echo "[Devinity] This is a working copy / developer branch"
	# for details, see Pivotal [#112288769]
	echo "[Devinity] Restarting celery instances from $APPDIR"
	# ps -fea | grep devinityc.$current_directory | grep -v grep | awk '{ printf $2" "}' | xargs kill -9
	supervisorctl -c ../master/supervisord.conf restart celeryd-$current_directory
	echo "[Devinity] Done"
fi

# Flower app moved to /etc/rc.local
# - we need only one instance per machine, not per application
# echo "Starting real-time celery monitor"
# flower --broker=$RUBI_BROKER_URL --basic_auth=$RUBI_BASIC_AUTH --port=$RUBI_PORT --log_file_prefix=$LOGFLOWER &
# echo "Done"

netstat -ano | grep ':5555\s' | if grep -q LISTEN; then echo $RUBI_MON_UP; else echo $RUBI_MON_DOWN; fi





echo "[Devinity] Starting Devinity gunicorn server from $APPDIR"
if [ "$current_directory" == "master" ]
then
    pyagent run -c /etc/appdynamics.cfg -- /usr/local/bin/gunicorn -c gunicorn_cfg.py $APPNAME.wsgi:application \
    --log-level=debug \
    --log-file=$LOGFILE 2>>$LOGFILE  1>>$ERRORFILE &
else
    /usr/local/bin/gunicorn -c gunicorn_cfg.py $APPNAME.wsgi:application \
    --log-level=debug \
    --log-file=$LOGFILE 2>>$LOGFILE  1>>$ERRORFILE &
fi
echo "[Devinity] Done"

touch startmark

exit 0
